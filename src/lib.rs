use seed::{prelude::*, *};

fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.stream(streams::window_event(Ev::Scroll, |_| Msg::Scrolled));
    orders.stream(streams::window_event(Ev::Resize, |_| Msg::Resized));
    Model {
        position: 0,
        screen_width: width(),
        screen_height: height(),
    }
}

struct Model {
    position: i32,
    screen_width: i32,
    screen_height: i32,
}

enum Msg {
    Scrolled,
    Resized,
}

fn width() -> i32 {
    match window().inner_width() {
        Ok(value) => {
            match value.as_f64() {
                Some(value) => value.round() as i32,
                _ => 0,
            }
        },
        _ => 0
    }
}

fn height() -> i32 {
    match window().inner_height() {
        Ok(value) => {
            match value.as_f64() {
                Some(value) => value.round() as i32,
                _ => 0,
            }
        },
        _ => 0
    }
}

fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        Msg::Scrolled => {
            let mut position = body().scroll_top();
            if position == 0 {
                position = document()
                    .document_element()
                    .expect("get document element")
                    .scroll_top()
            };
            model.position = position;
        },
        Msg::Resized => {
            model.screen_width = width();
            model.screen_height = height();
        },
    }
}

fn view(model: &Model) -> Node<Msg> {
    use std::cmp::{min, max};

    let starting_scale_play = 4.0 + ( f64::from(min(model.screen_width - 510, 0)) / 70.0);
    let starting_scale_play = if starting_scale_play < 2.0 { 2.0 } else {starting_scale_play};
    let starting_play_y = (model.screen_height / 2) - ( (25.0 * starting_scale_play) as i32);
    let starting_play_x = (model.screen_width / 2 ) - ( (25.0 * starting_scale_play) as i32);

    let starting_seed_y = model.screen_height / 2;
    let starting_seed_y = if starting_seed_y < 220 {
        starting_seed_y - (220 - starting_seed_y)
    } else { starting_seed_y };
    let starting_scale_seed = 0.5 + ( f64::from(min(model.screen_width - 500, 0)) / 500.0);
    let starting_scale_seed = if starting_scale_seed < 0.1 { 0.1 } else {starting_scale_seed};
    let starting_seed_x = ( model.screen_width / 2 ) - ( (500.0 * starting_scale_seed).round() as i32);

    let play_time = model.position;
    let translate_play_x = starting_play_x - play_time;
    let translate_play_x = max(translate_play_x, 2);
    let translate_play_y = starting_play_y - play_time;
    let translate_play_y = max(translate_play_y, 2);
    let scale_play_speed = 0.05;
    let scale_play = starting_scale_play - ( f64::from(play_time) * scale_play_speed);
    let scale_play = if scale_play < 2.0 { 2.0 } else {scale_play};

    let seed_time = model.position;
    let translate_seed_x = starting_seed_x - seed_time;
    let translate_seed_x = max(translate_seed_x, if seed_time > 50 { 150 } else { 0 });
    let translate_seed_y = starting_seed_y - seed_time;
    let translate_seed_y = max(translate_seed_y, 0);
    let scale_seed_speed = 0.05;
    let scale_seed = starting_scale_seed - ( f64::from(seed_time) * scale_seed_speed);
    let scale_seed = if scale_seed < 0.15 { 0.15 } else {scale_seed};
    
    div![
        div![
            svg! [
                style!{ 
                    "position" => "fixed", 
                    "top" => "0", 
                    "left" => "0", 
                    "height" => "100%", 
                    "width" => "100%",
                    "padding" => "1rem",
                    "z-index" => "-10",
                },
                g![ 
                    attrs!{
                        "transform" => format!("translate({} {}) scale({})", translate_play_x, translate_play_y, scale_play),
                    },
                    path! [
                        attrs! {
                            "d" => "m 2.6473093,31.040343 c 0.604319,-2.872444 1.445139,-5.691905 2.249467,-8.513443 1.249137,-4.547877 2.135153,-9.186431 2.888382,-13.8390836 0.441592,-2.642261 0.675142,-5.311047 0.983236,-7.97007197 0.01558,-0.07755 0.116256,-0.270471 0.04678,-0.232651 -0.830316,0.451996 -1.664631,0.91744797 -2.398811,1.51301997 -0.09815,0.07962 0.05224,0.247324 0.07569,0.371512 0.116303,0.616061 0.220086,1.234583 0.33474,1.850988 0.416793,1.903494 0.935175,3.782544 1.531133,5.637228 0.450882,1.4584806 1.027298,2.8731466 1.604383,4.2850136 0.2876227,0.752071 0.6698537,1.462858 0.9212867,2.228607 0.0504,0.397314 0.380339,0.774769 0.231905,1.174792 -0.810022,0.480862 -1.8819357,1.165646 1.882756,-1.116213 0.07671,-0.04649 -0.160959,0.0793 -0.24285,0.1159 -0.180798,0.0808 -0.331232,0.135213 -0.517017,0.206232 -0.614426,0.210093 -1.234967,0.400947 -1.845442,0.62248 -0.6840507,0.263226 -1.3284407,0.614974 -1.9717517,0.96319 -1.156515,0.628681 -2.288995,1.296257 -3.423248,1.966301 -0.331885,0.196056 -0.635884,0.445458 -0.95513,0.661342 -0.08835,0.06217 -0.176704,0.124335 -0.265054,0.186505 0,0 2.592147,-1.021155 2.592147,-1.021155 v 0 c 0.08723,-0.06085 0.174468,-0.1217 0.261704,-0.182549 0.09006,-0.06297 0.526082,-0.368443 0.605153,-0.420947 0.114067,-0.07574 0.465828,-0.287721 0.346321,-0.22089 -3.790863,2.119958 -2.165128,1.241611 -1.325632,0.775753 0.637038,-0.354263 1.278089,-0.703863 1.947029,-0.995108 0.603145,-0.230508 1.218719,-0.427022 1.8363327,-0.614193 1.33812,-0.495165 1.730436,-0.797865 3.385235,-1.801284 0.06343,-0.03846 0.108776,-0.103468 0.14713,-0.166962 0.03474,-0.05751 0.04552,-0.126431 0.06828,-0.189648 0.02479,-0.459224 -0.204422,-0.854753 -0.31551,-1.292154 C 13.050089,14.261217 12.661323,13.549165 12.363392,12.796253 11.760131,11.40705 11.154797,10.015607 10.711307,8.5648734 10.11591,6.7195024 9.5864843,4.8481464 9.2350883,2.9394714 c -0.106616,-0.624038 -0.197,-1.249474 -0.29601,-1.874594 -0.02265,-0.14290897 0.0655,-0.46303397 -0.07478,-0.42758297 -2.62269,0.66282097 -2.554184,0.201729 -2.558399,1.31097397 -0.103428,2.624058 -0.383069,5.238591 -0.761304,7.836606 -0.740225,4.6501626 -1.605653,9.2880916 -2.913081,13.8170046 -0.844799,2.878859 -1.72050596,5.749922 -2.49735196,8.647991 0,0 2.51314996,-1.209527 2.51314996,-1.209527 z",
                            "style" => "fill:#000000;fill-opacity:1;stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none;paint-order:stroke fill markers",
                        }
                    ],
                    path! [
                        attrs! {
                            "d" => "m 21.376304,1.4279104 c -0.358841,2.438606 -0.722802,4.876554 -1.071788,7.316906 -0.643207,4.7392276 -1.7581,9.3947186 -2.922357,14.0277746 -0.427202,1.569706 -0.862118,3.141419 -1.369002,4.687908 -0.297926,0.90896 -0.660252,1.79533 -0.979839,2.696626 -0.03712,0.226303 -0.223663,0.690626 0.12923,0.857003 0.53657,0.252971 1.552379,0.04719 2.025838,-0.01545 1.189405,-0.157337 2.374466,-0.345956 3.562339,-0.514461 3.563938,-0.483939 7.155336,-0.700199 10.745944,-0.859396 0,0 2.06226,-1.514388 2.06226,-1.514388 v 0 c -3.597246,0.205846 -7.19669,0.422447 -10.775317,0.854874 -1.155518,0.146685 -2.309448,0.305814 -3.46537,0.449284 -0.379574,0.04711 -0.76005,0.08666 -1.14032,0.127788 -0.203419,0.02199 -0.407897,0.08857 -0.610716,0.0616 -0.267864,-0.03562 -0.06844,-0.60048 -0.06807,-0.602954 0.290412,-0.890413 0.617365,-1.769248 0.888357,-2.665963 0.464447,-1.536853 0.865425,-3.094201 1.261435,-4.649578 1.114346,-4.654955 2.177463,-9.325496 2.927125,-14.0555236 0.394645,-2.48303 0.811355,-4.963028 1.31436,-7.42661897 0,0 -2.51411,1.22455797 -2.51411,1.22455797 z",
                            "style" => "fill:#000000;fill-opacity:1;stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none;paint-order:stroke fill markers",
                        }
                    ],
                    path! [
                        attrs! {
                            "d" => "m 36.765378,30.734862 c 0.720122,-1.442884 1.281769,-2.959418 1.903704,-4.445225 0.583647,-1.394333 1.613437,-3.740274 2.202582,-5.091081 0.389726,-0.906187 0.78296,-1.810874 1.169178,-2.718562 1.431345,-3.363936 0.858472,-2.093423 2.292389,-5.389713 0.975564,-2.242627 1.958642,-4.4741776 2.873987,-6.7422676 0.57129,-1.33848 1.03406,-2.730122 1.681014,-4.036637 0.07199,-0.145381 0.396086,-0.485325 0.248205,-0.418629 -2.3974,1.08127 -2.20268,0.521777 -2.442945,1.685705 -0.456668,2.976706 -0.74258,5.970495 -0.968169,8.9725046 -0.230489,3.454342 -0.339767,6.915208 -0.383889,10.376485 -0.04342,1.891773 0.01815,3.786835 -0.124177,5.674984 0.581179,-0.247666 -0.390025,0.753356 -0.195262,0.642821 0.759052,-0.430789 1.506908,-0.886777 2.211141,-1.402331 0.05772,-0.04225 -0.145378,-0.02593 -0.203787,-0.0672 -0.347843,-0.245864 -1.006473,-0.926375 -1.249305,-1.165151 -2.222304,-2.202794 -4.638251,-4.19948 -7.055281,-6.182794 -0.61314,-0.550855 -1.290103,-1.024242 -1.917372,-1.557756 -0.206475,-0.175617 -0.434057,-0.335118 -0.59823,-0.550804 -0.06985,-0.09177 0.02365,-0.392279 -0.07764,-0.33715 -0.643734,0.35035 -1.573786,0.620111 -1.749589,1.33161 -0.115096,0.465807 0.958506,-0.04639 1.437759,-0.06959 4.664639,0.09986 9.323795,0.421314 13.971572,0.820486 0,0 1.980438,-1.415235 1.980438,-1.415235 v 0 c -4.609463,-0.330473 -9.221055,-0.632246 -13.831679,-0.944944 -2.165945,-0.119626 -1.767734,-0.35751 -4.233674,1.623163 -0.121732,0.09777 0.09882,0.305472 0.20211,0.422556 0.185488,0.210254 0.426008,0.364871 0.645961,0.538744 0.649941,0.513776 1.335112,0.982229 1.965521,1.521265 2.419863,1.952955 4.845989,3.920191 6.975128,6.195224 0.395833,0.431646 0.768686,0.886174 1.19284,1.290029 0.08829,0.08406 0.179469,0.268292 0.291666,0.220628 2.188938,-0.929963 2.572938,-0.672476 2.697152,-2.149806 0.06395,-1.873991 -0.03645,-3.749363 -0.0033,-5.624042 0.024,-3.441906 0.06389,-6.886006 0.311753,-10.320615 0.133594,-1.6608496 0.171604,-2.3474836 0.371412,-3.9929776 0.207476,-1.708623 0.529323,-3.3978 0.810985,-5.094792 -0.04933,-1.43348897 0.135845,-0.949714 -2.550777,0.697317 -0.124391,0.07626 -0.11485,0.268295 -0.168669,0.40391 -0.248983,0.627428 -0.63423,1.653895 -0.852559,2.228416 -0.219943,0.578771 -0.44038,1.157354 -0.660569,1.736032 -1.952342,4.9829216 -4.16547,9.8598696 -6.235491,14.7943666 -0.946597,2.115196 -1.369946,3.068421 -2.310413,5.142924 -0.606457,1.33773 -1.192773,2.702681 -1.87162,4.007667 -0.09396,0.180631 -0.199858,0.354798 -0.299789,0.532196 0,0 2.547721,-1.141727 2.547721,-1.141727 z",
                            "style" => "fill:#000000;fill-opacity:1;stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none;paint-order:stroke fill markers",
                        }
                    ],
                    path! [
                        attrs! {
                            "d" => "m 53.400912,2.8629314 c 0.793406,1.945354 1.451991,3.944037 2.170613,5.918221 0.973188,2.5934346 2.002898,5.1667506 3.103851,7.7086686 0.277243,0.429477 0.488923,1.442596 1.150778,1.539391 0.159438,0.02331 0.338852,-0.0053 0.474509,-0.0923 2.968393,-1.902738 2.353903,-1.147884 3.434373,-3.055038 1.325826,-2.989966 2.622816,-5.9907056 4.020016,-8.9486466 0.39804,-0.861026 0.56432,-1.241251 0.96851,-2.048166 0.53728,-1.072613 1.20835,-1.768343 -2.32375,0.781783 -0.24767,0.178818 -0.21842,0.570632 -0.33338,0.853657 -0.20158,0.496263 -0.41081,0.98939 -0.61622,1.484087 -1.215316,2.714138 -2.364516,5.4634736 -3.678566,8.1324946 -0.95782,1.945476 -1.19866,2.310072 -2.259526,4.187746 -1.767109,2.970035 -3.849391,5.736452 -6.13751,8.321789 -0.948412,1.038822 -1.964899,2.016183 -3.006947,2.960523 0,0 2.544805,-1.042993 2.544805,-1.042993 v 0 c 0.999048,-0.950227 1.991436,-1.910945 2.901936,-2.94762 2.224998,-2.587447 4.230667,-5.357053 5.959632,-8.301989 0.95543,-1.711344 1.39081,-2.439459 2.26052,-4.208521 1.323896,-2.692921 2.465846,-5.4742366 3.765876,-8.1786116 0.23645,-0.508802 0.46905,-1.019408 0.70933,-1.526405 0.1465,-0.309105 0.79186,-0.950069 0.45097,-0.921787 -0.90883,0.0754 -1.71484,0.64286 -2.50825,1.092464 -0.1343,0.0761 -0.1143,0.286835 -0.17571,0.428459 -0.29215,0.673851 -0.60698,1.337743 -0.89308,2.014275 -1.359426,2.981352 -2.645536,5.9975856 -4.078586,8.9443136 -0.21012,0.330798 -1.012793,0.906785 -0.63037,0.992394 0.63078,0.141205 1.18058,-0.533565 1.79592,-0.731478 0.13692,-0.04403 -0.20299,0.225031 -0.34152,0.263705 -0.11356,0.03171 -0.252,-0.0011 -0.34638,-0.07165 -0.27864,-0.20864 -0.66904,-1.009055 -0.79068,-1.228077 -1.163249,-2.506848 -2.16973,-5.082844 -3.117591,-7.6780806 -0.711952,-1.994739 -1.387785,-4.001196 -2.041004,-6.015773 0,0 -2.432569,1.373169 -2.432569,1.373169 z",
                            "style" => "fill:#000000;fill-opacity:1;stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none;paint-order:stroke fill markers",
                        }
                    ],
                    path! [
                        attrs! {
                            "transform" => "matrix(0.26458333,0,0,0.26458333,0.13416234,0.20334143)",
                            "d" => "m 21.114029,73.063753 c 1.171035,-3.820667 5.735945,-26.609261 7.188796,-35.887364 l 0.893977,-5.709057 1.855698,5.709057 c 1.020634,3.139981 3.734242,10.357657 6.030241,16.03928 2.70276,6.688185 3.966188,10.399674 3.58357,10.527214 -1.517915,0.505971 -10.874399,5.284127 -14.903103,7.610694 -2.470542,1.426733 -4.588238,2.594059 -4.705992,2.594059 -0.117753,0 -0.09219,-0.397747 0.05681,-0.883883 z",
                            "style" => "fill:#00cf00;fill-opacity:0.56470591;stroke-width:0.7937008;stroke-miterlimit:4;stroke-dasharray:none;paint-order:stroke fill markers;stroke:#00cf00;stroke-opacity:0.56470591;fill-rule:nonzero",
                        }
                    ]
                ],
                g![
                    attrs! {
                        "transform" => format!("translate({} {}) scale({})", translate_seed_x, translate_seed_y, scale_seed),
                    },
                    style!{ 
                        "fill-rule" => "evenodd", 
                        "clip-rule" => "evenodd", 
                        "stroke-linejoin" => "round",
                        "stroke-miterlimit" => "1.41421",
                    },
                    path! [
                        attrs!{
                            "style" => "stroke-width:0.99983048",
                            "id" => "d",
                            "d" => "M 945.99663,18.501675 V 386.98904 h -42.99716 v -19.72821 c -17.08747,12.48896 -37.60714,19.74022 -60.37602,19.74022 -57.05323,0 -103.37418,-46.31136 -103.37418,-103.35378 0,-57.04242 46.32095,-103.35378 103.37418,-103.35378 22.52851,0 43.38714,7.22203 60.37602,19.47469 V 18.501675 Z M 842.62345,223.28076 c 33.3278,0 60.37602,27.04961 60.37602,60.36651 0,33.3169 -27.04822,60.36652 -60.37602,60.36652 -33.3178,0 -60.37801,-27.04962 -60.37801,-60.36652 0,-33.3169 27.06021,-60.36651 60.37801,-60.36651 z",
                        }
                    ],
                    g![
                        attrs!{
                            "id" => "e2",
                            "transform" => "matrix(0.999934,0,0,0.999727,77.005752,-225.73563)",
                        },
                        path! [
                            attrs!{
                                "id" => "path4776",
                                "d" => "m 637.757,531.022 c -9.905,46.754 -51.452,81.882 -101.139,81.882 -57.059,0 -103.383,-46.324 -103.383,-103.382 0,-57.058 46.324,-103.382 103.383,-103.382 49.687,0 91.234,35.128 101.139,81.882 h -44.703 c -8.671,-22.724 -30.682,-38.883 -56.436,-38.883 -25.755,0 -47.765,16.159 -56.437,38.883 h 56.437 l 16.591,21.5 -16.591,21.5 h -56.437 c 8.672,22.724 30.682,38.883 56.437,38.883 25.754,0 47.765,-16.159 56.436,-38.883 z",
                            },
                        ]
                    ],
                    g![
                        attrs! {
                            "id" => "e1",
                            "style" => "clip-rule:evenodd;fill-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41420996",
                            "transform" => "matrix(0.999934,0,0,0.999727,-152.03136,-225.73563)",
                        },
                        path! [
                            attrs! {
                                "id" => "path4780",
                                "d" => "m 637.757,531.022 c -9.905,46.754 -51.452,81.882 -101.139,81.882 -57.059,0 -103.383,-46.324 -103.383,-103.382 0,-57.058 46.324,-103.382 103.383,-103.382 49.687,0 91.234,35.128 101.139,81.882 h -44.703 c -8.671,-22.724 -30.682,-38.883 -56.436,-38.883 -25.755,0 -47.765,16.159 -56.437,38.883 h 56.437 l 16.591,21.5 -16.591,21.5 h -56.437 c 8.672,22.724 30.682,38.883 56.437,38.883 25.754,0 47.765,-16.159 56.436,-38.883 z",
                            }
                        ]
                    ],
                    path! [
                        attrs! {
                            "style" => "stroke-width:1.06388259",
                            "d" => "m 222.40664,101.38268 -39.68497,5.17423 C 180.76219,92.09029 173.79685,79.155739 162.71239,69.912734 l -27.0677,26.788806 c -4.75466,4.70535 -12.4429,4.70535 -17.19756,0 L 91.379424,69.912734 c -12.897461,10.352173 -21.1518,26.192826 -21.1518,43.933146 0,31.15705 25.459389,56.4522 56.818756,56.4522 v 0.002 l 10.82989,0.75707 c 4.7331,0.52333 9.27875,1.22452 13.88251,2.43509 l 16.33809,-12.5051 3.59435,20.17726 c 4.22324,2.18739 11.86109,6.11448 15.71318,8.89598 l 23.21583,-9.58839 -2.55086,22.96372 c 3.56863,4.06148 9.33917,9.81373 12.26941,14.35603 l 23.33335,1.93501 -12.51681,19.78297 c 1.85986,5.07527 3.34094,10.28136 4.43259,15.57576 l 21.05854,10.23137 -18.8199,13.9269 c -0.099,5.40397 -0.59902,10.79304 -1.49384,16.12361 l 15.93861,17.14695 -22.58006,6.18902 c -2.04499,5.00398 -4.45812,9.84841 -7.21919,14.49603 l 8.6673,21.74564 -23.29187,-2.38392 c -3.7144,3.92638 -7.71501,7.57193 -11.96886,10.90899 l 0.22557,23.40724 -20.85745,-10.63453 c -4.88372,2.31903 -9.93131,4.27424 -15.10338,5.84969 l -8.24701,21.90733 -15.60664,-17.45012 c -5.39126,0.39997 -10.80485,0.39997 -16.1961,0 l -15.60664,17.44985 -8.248075,-21.90733 c -5.171004,-1.57545 -10.21965,-3.53065 -15.102312,-5.84968 L 60.279154,386.87705 60.50472,363.4698 c -4.253842,-3.33706 -8.254455,-6.9826 -11.969918,-10.90898 l -23.291865,2.38391 8.667285,-21.74564 C 31.150225,328.5516 28.737089,323.70718 26.691031,318.7032 L 4.1109778,312.51417 20.050653,295.36722 c -0.895882,-5.33057 -1.394894,-10.71963 -1.49491,-16.12361 l 0.03831,-3.12429 H 63.69883 c -0.0064,0.36593 -0.0096,0.73188 -0.0096,1.09887 0,19.4213 8.605573,36.85015 22.209784,48.67717 l 32.508176,-32.37485 c 5.41039,-5.38801 14.15855,-5.38801 19.56788,0 l 32.50818,32.37485 c 13.6042,-11.82702 22.21084,-29.25587 22.21084,-48.67717 0,-35.59275 -28.9023,-64.48908 -64.50243,-64.49014 -2.70254,0 -5.40295,-0.099 -8.09805,-0.29892 l -15.60664,17.45012 -8.248074,-21.90839 c -5.171005,-1.57438 -10.21965,-3.52959 -15.102313,-5.84968 l -20.857449,10.6356 7.143289,-23.40726 c -3.747115,-2.92117 -7.271166,-6.11333 -10.544034,-9.5504 l -20.517283,2.08683 7.634816,-19.03562 c -2.431219,-4.06748 -4.556896,-8.3091 -6.359223,-12.68854 l -19.890268,-5.41773 14.040904,-15.01003 c -0.789162,-4.66625 -1.22873,-9.38371 -1.316833,-14.11422 L 13.89343,103.42853 32.443431,94.47131 c 0.960679,-4.63366 2.266265,-9.190963 3.903636,-13.633724 L 25.322225,63.520043 45.876062,61.826189 c 2.581179,-3.97623 5.45478,-7.757837 8.597371,-11.312232 l -3.98424,-20.105564 19.781544,5.797659 c 3.853025,-2.781498 7.907557,-5.276185 12.129857,-7.462645 L 85.995875,8.565209 102.33397,21.07124 c 4.60377,-1.210561 9.29094,-2.081234 14.02403,-2.60457 L 127.04638,0.941465 137.73475,18.46667 c 4.73216,0.523336 9.42027,1.394009 14.02403,2.60457 l 16.33809,-12.506031 3.59435,20.178198 c 4.22324,2.18646 8.27776,4.681147 12.12985,7.462645 l 19.78249,-5.797659 -3.98518,20.105564 c 3.14353,3.554395 6.01713,7.336002 8.59831,11.312232 l 20.55384,1.693854 -11.02579,17.317543 c 2.54496,8.078974 3.8533,13.222524 4.6619,20.545094 z m -94.21496,249.0454 c -8.98754,0 -16.28441,-7.29535 -16.28441,-16.28104 0,-8.98568 7.29687,-16.28104 16.28441,-16.28104 8.98754,0 16.28441,7.29536 16.28441,16.28104 0,8.98569 -7.29687,16.28104 -16.28441,16.28104 z m -1.1453,-303.561638 c 7.91692,0 14.34456,6.386177 14.34456,14.252034 0,7.865855 -6.42764,14.252029 -14.34456,14.252029 -7.91693,0 -14.34457,-6.386174 -14.34457,-14.252029 0,-7.865857 6.42764,-14.252034 14.34457,-14.252034 z",
                            "id" => "S",
                            "sodipodi:nodetypes" => "ccccccsccccccccccccccccccccccccccccccccccccccccccsccccsccccccccccccccccccccccccccccccccccccccssssssssss",
                        }
                    ]
                ]
            ]
        ],
        main![
            style! {
                "background-color" => "white",
            },
            div![
                style! {
                    "margin-top" => format!("{}px", model.screen_height),
                    "max-width" => "750px",
                    "margin-left" => "auto",
                    "margin-right" => "auto",
                },
                h2! ["About"],
                p! [
                    a![ attrs!{ "href" => "https://seed-rs.org/" }, "Seed" ], 
                    " is a frontend Rust framework for creating fast and reliable web apps with an elm-like architecture.",
                ],
                p! [
                    " Play Seed is a playground, where you can write simple Seed based application in the browser, build it and run.",
                    " This is intended to be a platform to share frontend ideas, demos, etc, specifically for use with Seed."
                ], 
                h2! ["Links"],
                p! [
                    a![
                        attrs! { "href" => "https://ide.play-seed.dev/" }, 
                        "Playground"
                    ],
                ],
                p! [
                    a![
                        attrs! { "href" => "https://docs.play-seed.dev/" }, 
                        "Documentation"
                    ],
                ],
                p! [
                    a![
                        attrs! { "href" => "https://seed-rs.org/" }, 
                        "Official Seed framework site"
                    ],
                ],
                h2! [ "How does it work?" ],
                p![
                    "In the ",
                    a![
                        attrs! { "href" => "https://ide.play-seed.dev/" }, 
                        "playground"
                    ],
                    " you will see three buttons: 'Run', 'Build' and 'Build & Run'. ",
                ], 
                p![
                    "'Build' would cause '.play-seed/build.ts' script execution, which by default would grab 'src' folder ",
                    "and send it to backend for compilation. There is predefined Cargo.toml file, which backend would use ",
                    "to compile received sources with standard 'wasm-pack' call, the results of which (wasm and js files) ",
                    "would be returned back to the frontend app and put in the 'out' folder in the playground. ",
                    "Also, once build is finished you will see stdout and stderr results in the Output panel. "
                ],
                p![
                    "'Run' would grab '.play-seed/main.html' and use it as an entrypoint, populating iframe with it's content ",
                    "and additionally loading any JS or CSS files linked from it into same iframe. This loading process, however, is ",
                    "currently imperfect (more about that in docs). By default 'main.html' would link 'main.js', which in its turn ",
                    "would load wasm-bindgen built js from 'out/index.js' and pass loaded 'out/main.wasm' to it."
                ],
                p![
                    "As you have probably guessed, 'Build & Run' would simply run both 'Build' and 'Run' sequentially. ",
                    "As build process might be somewhat long, if you don't change Rust code, you could use only 'Run' to see ",
                    "new result.", 
                ],
                p![
                    "Additionally you might need to know, that currently backend only allows three simultaneous builds running, ",
                    "as they are somewhat heavy and compute capabilities are quite limited. In case if limit is exceeded you will see ",
                    "message 'max connections reached' in the Output." 
                ],
                h2! [ "Sources" ],
                p![
                    "Currenty sources are available on the Gitlab: ", 
                ],
                p! [
                    a![
                        attrs! { "href" => "https://gitlab.com/strwrite/seed-playground-ide" }, 
                        "Frontend"
                    ],
                ],
                p! [
                    a![
                        attrs! { "href" => "https://gitlab.com/strwrite/seed-playground" }, 
                        "Backend"
                    ] 
                ],
                h2! [ "Acknowledgements" ],
                p! [
                    "Play Seed has been forked from two base sources:"
                ],
                p! [
                    a![
                        attrs! { "href" => "https://github.com/wasdk/WebAssemblyStudio" }, 
                        "WebAssemblyStudio"
                    ], " used for a frontend"
                ],
                p! [
                    a![
                        attrs! { "href" => "https://github.com/integer32llc/rust-playground" }, 
                        "Rust Playground"
                    ], " used for a backend"
                ],
            ]
        ]
    ]
}

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}
